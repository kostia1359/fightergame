import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';
import {IReducedFighter} from './helpers/fighterTypes';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement:HTMLElement|null = document.getElementById('root');
  static loadingElement:HTMLElement|null = document.getElementById('loading-overlay');

  async startApp():Promise<void> {
    if (App.loadingElement === null || App.rootElement === null) {
      console.warn('Failed to find root elements');
      return;
    }

    try {
      App.loadingElement.style.visibility = 'visible';

      const fighters:IReducedFighter[] = await fighterService.getFighters();
      const fightersElement:HTMLElement = createFighters(fighters);

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;
