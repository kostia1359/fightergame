import { callApi } from '../helpers/apiHelper';
import { IFighter, IReducedFighter } from '../helpers/fighterTypes';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult: IReducedFighter[] = await callApi<IReducedFighter[]>(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw <Error>error;
    }
  }

  async getFighterDetails(id: string) {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult: IFighter = await callApi<IFighter>(endpoint, 'GET');

      return apiResult;
    } catch (e) {
      throw <Error>e;
    }
  }
}

export const fighterService = new FighterService();
