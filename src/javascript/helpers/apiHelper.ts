import { IFighter } from './fighterTypes';
import {IGitApi} from './standartInterface';
import { fightersDetails, fighters } from './mockData';

const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI = true;

async function callApi<T>(endpoint: string, method: 'GET' | 'POST' | 'PUT' | 'DELETE'):Promise<T> {
  const url = API_URL + endpoint;
  const options = {
    method
  };

  return useMockAPI
    ? fakeCallApi<T>(endpoint)
    : fetch(url, options)
      .then((response):Promise<IGitApi> => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
      .then((result):Promise<T> => JSON.parse(atob(result.content)))
      .catch((error:Error) => {
        throw error;
      });
}

async function fakeCallApi<T>(endpoint: string): Promise<T> {
  const response = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(<T><unknown>response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint: string): IFighter | undefined {
  const start = endpoint.lastIndexOf('/');
  const end = endpoint.lastIndexOf('.json');
  const id = endpoint.substring(start + 1, end);

  return fightersDetails.find((it) => it._id === id);
}

export { callApi };
