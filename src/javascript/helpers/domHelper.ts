import { IDictionary } from './standartInterface';

type elementData = {
  tagName: string,
  className?: string,
  attributes?: IDictionary };

export function createElement({ tagName, className, attributes = {} }:elementData):HTMLElement {
  const element: HTMLElement = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key:string) => element.setAttribute(key, attributes[key]));

  return element;
}
