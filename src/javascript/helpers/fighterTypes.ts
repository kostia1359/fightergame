export interface IReducedFighter {
  _id: string,
  name: string,
  source: string
}

export interface IFighter extends IReducedFighter {
  attack: number,
  defense: number,
  health: number
}

export type fighterPositions = 'left' | 'right';
export type selectFunction=(event:Event,id:string)=>Promise<void>;
export  type fighterTuple = [IFighter, IFighter];
