import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
// @ts-ignore
import versusImg from '../../../resources/versus.png';
import { IFighter } from '../helpers/fighterTypes';
import {fighterTuple,selectFunction} from '../helpers/fighterTypes';


export function createFightersSelector(): selectFunction{
  let selectedFighters: IFighter[] = [];

  return async (event: Event, fighterId: string) => {
    const fighter: IFighter | undefined = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter: IFighter = playerOne ?? fighter;
    const secondFighter: IFighter = playerOne ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];
    const selectedFightersTuple: fighterTuple = [selectedFighters[0], selectedFighters[1]];

    renderSelectedFighters(selectedFightersTuple);
  };
}

const fighterDetailsMap = new Map<string, IFighter>();

export async function getFighterInfo(fighterId: string): Promise<IFighter> {
  if (fighterDetailsMap.has(fighterId)) {
    return fighterDetailsMap.get(fighterId) as IFighter;
  }
  const fighterDetails: IFighter = await fighterService.getFighterDetails(fighterId);

  fighterDetailsMap.set(fighterId, fighterDetails);

  return fighterDetails;
}

function renderSelectedFighters(selectedFighters: fighterTuple): void {
  const fightersPreview: HTMLElement | null = document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview: HTMLElement = createFighterPreview(playerOne, 'left');
  const secondPreview: HTMLElement = createFighterPreview(playerTwo, 'right');
  const versusBlock: HTMLElement = createVersusBlock(selectedFighters);

  if (fightersPreview === null) return;
  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: fighterTuple): HTMLElement {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container: HTMLElement = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image: HTMLElement = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg }
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn: HTMLElement = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: fighterTuple): void {
  renderArena(selectedFighters);
}
