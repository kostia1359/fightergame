import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { IFighter, fighterPositions, fighterTuple } from '../helpers/fighterTypes';


export function renderArena(selectedFighters: fighterTuple): void {
  const root: HTMLElement | null = document.getElementById('root');
  const arena: HTMLElement = createArena(selectedFighters);
  const [firstFighter, secondFighter] = selectedFighters;

  if (root === null) return;
  root.innerHTML = '';
  root.append(arena);

  fight(firstFighter, secondFighter).then(winner => showWinnerModal(winner));
}

function createArena(selectedFighters: fighterTuple): HTMLElement {
  const arena: HTMLElement = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators: HTMLElement = createHealthIndicators(...selectedFighters);
  const fighters: HTMLElement = createFighters(...selectedFighters);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter: IFighter, rightFighter: IFighter): HTMLElement {
  const healthIndicators: HTMLElement = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign: HTMLElement = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator: HTMLElement = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator: HTMLElement = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: IFighter, position: fighterPositions): HTMLElement {
  const { name } = fighter;
  const container: HTMLElement = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName: HTMLElement = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator: HTMLElement = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar: HTMLElement = createElement({
    tagName: 'div',
    className: 'arena___health-bar',
    attributes: { id: `${position}-fighter-indicator` }
  });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter: IFighter, secondFighter: IFighter): HTMLElement {
  const battleField: HTMLElement = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement: HTMLElement = createFighter(firstFighter, 'left');
  const secondFighterElement: HTMLElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: IFighter, position: fighterPositions): HTMLElement {
  const imgElement: HTMLElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
