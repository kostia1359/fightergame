import { createElement } from '../helpers/domHelper';
import { IFighter } from '../helpers/fighterTypes';
import { IDictionary } from '../helpers/standartInterface';
import { fighterPositions } from '../helpers/fighterTypes';

export function createFighterPreview(fighter: IFighter, position: fighterPositions): HTMLElement {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  if (fighter === undefined) {
    return fighterElement;
  }

  const fighterPreview: HTMLElement = createFighterPreviewElement(fighter, position);

  fighterElement.append(fighterPreview);

  return fighterElement;
}

export function createFighterImage(fighter: IFighter): HTMLElement {
  const { source, name } = fighter;
  const attributes: IDictionary = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement: HTMLElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}

function createCharacteristicElement(characteristicName: string, value: number): HTMLElement {
  const element: HTMLElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___characteristic'
  });
  const characteristicNameElement: HTMLElement = createElement({
    tagName: 'p',
    className: 'fighter-preview___characteristicName'
  });
  const characteristicValueElement: HTMLElement = createElement({
    tagName: 'p',
    className: 'fighter-preview___characteristicValue'
  });
  characteristicValueElement.innerText = value.toString();
  characteristicNameElement.innerText = characteristicName + ':';

  element.append(characteristicNameElement);
  element.append(characteristicValueElement);

  return element;
}

function createImageElement(fighter: IFighter, position: fighterPositions): HTMLElement {
  const imgElement: HTMLElement = createFighterImage(fighter);

  imgElement.className += ' fighter-preview___image';
  if (position === 'right') {
    imgElement.style.transform = 'scale(-1, 1)';
  }

  return imgElement;
}

function createNameElement(name: string): HTMLElement {
  const nameElement: HTMLElement = createElement({
    tagName: 'h1',
    className: 'fighter-preview___name'
  });

  nameElement.innerText = name;

  return nameElement;
}

function createFighterPreviewElement(fighter: IFighter, position: fighterPositions): HTMLElement {
  const { name, health, defense, attack } = fighter;

  const fighterWrapper: HTMLElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___wrapper'
  });
  const imgElement: HTMLElement = createImageElement(fighter, position);
  const nameElement: HTMLElement = createNameElement(name);
  const heathElement: HTMLElement = createCharacteristicElement('Health', health);
  const attackElement: HTMLElement = createCharacteristicElement('Attack', attack);
  const defenseElement: HTMLElement = createCharacteristicElement('Defense', defense);

  fighterWrapper.append(nameElement, imgElement, heathElement, attackElement, defenseElement);

  return fighterWrapper;
}
