import { controls } from '../../constants/controls';
import { IFighter, fighterPositions } from '../helpers/fighterTypes';

interface IArenaFighter {
  healthIndicator: HTMLElement,
  currentHealth: number,
  isCritAvailable: boolean,
  fighter: IFighter
}

export async function fight(firstFighter: IFighter, secondFighter: IFighter): Promise<IFighter> {
  return new Promise((resolve) => {
    const delayBeforeCrit = 10000;
    const leftFighter: IArenaFighter = createArenaFighter(firstFighter, 'left');
    const rightFighter: IArenaFighter = createArenaFighter(secondFighter, 'right');
    const pressedKeys: Set<string> = new Set();

    document.addEventListener('keyup', keyUpHandler);
    document.addEventListener('keydown', keyDownHandler);

    function resolvePromise(winner: IFighter): void {
      document.removeEventListener('keyup', keyUpHandler);
      document.removeEventListener('keydown', keyDownHandler);
      resolve(winner);
    }

    function keyUpHandler(keyEvent: KeyboardEvent): void {
      const keyCode = keyEvent.code;

      pressedKeys.delete(keyCode);
    }

    function createArenaFighter(fighter: IFighter, position: fighterPositions): IArenaFighter {
      return {
        fighter,
        healthIndicator: document.getElementById(`${position}-fighter-indicator`) as HTMLElement,
        currentHealth: fighter.health,
        isCritAvailable: true
      };
    }

    function makeSimpleAttack(attacker: IArenaFighter, defender: IArenaFighter): void {
      const damage = getDamage(attacker.fighter, defender.fighter);

      if (!isAttackSuccessful(attacker, defender, damage)) return;

      receiveDamage(attacker, defender, damage);
    }

    function isAttackSuccessful(attacker: IArenaFighter, defender: IArenaFighter, damage: number): boolean {
      const leftBlockCode = controls.PlayerOneBlock;
      const rightBlockCode = controls.PlayerTwoBlock;

      if (pressedKeys.has(leftBlockCode) || pressedKeys.has(rightBlockCode)) return false;

      return damage > 0;
    }

    function receiveDamage(attacker: IArenaFighter, defender: IArenaFighter, damage: number): void {
      defender.currentHealth -= damage;
      fillIndicator(defender);

      if (defender.currentHealth <= 0) resolvePromise(attacker.fighter);
    }

    function makeCritAttack(attacker: IArenaFighter, defender: IArenaFighter): void {
      if (!attacker.isCritAvailable) return;

      setCritTimer(attacker);

      receiveDamage(attacker, defender, attacker.fighter.attack * 2);
    }

    function setCritTimer(attacker: IArenaFighter): void {
      setTimeout(() => attacker.isCritAvailable = true, delayBeforeCrit);

      attacker.isCritAvailable = false;
    }

    function isLeftCritKeysPressed(pressedKeyCode: string): boolean {
      let isCritKeysPressed: boolean = controls.PlayerOneCriticalHitCombination.some(keyCode => keyCode === pressedKeyCode);

      controls.PlayerOneCriticalHitCombination.forEach(keyCode => {
        if (!pressedKeys.has(keyCode)) isCritKeysPressed = false;
      });

      return isCritKeysPressed;
    }

    function isRightCritKeysPresse(pressedKeyCode: string): boolean {
      let isCritKeysPressed: boolean = controls.PlayerTwoCriticalHitCombination.some(keyCode => keyCode === pressedKeyCode);

      controls.PlayerTwoCriticalHitCombination.forEach(keyCode => {
        if (!pressedKeys.has(keyCode)) isCritKeysPressed = false;
      });

      return isCritKeysPressed;
    }

    function makeAttack(pressedKeyCode: string): void {
      pressedKeys.add(pressedKeyCode);

      switch (pressedKeyCode) {
        case controls.PlayerOneAttack:
          makeSimpleAttack(leftFighter, rightFighter);
          break;
        case controls.PlayerTwoAttack:
          makeSimpleAttack(rightFighter, leftFighter);
          break;
      }

      if (isLeftCritKeysPressed(pressedKeyCode) && !pressedKeys.has(controls.PlayerOneBlock)) {
        makeCritAttack(leftFighter, rightFighter);
      }

      if (isRightCritKeysPresse(pressedKeyCode) && !pressedKeys.has(controls.PlayerTwoBlock)) {
        makeCritAttack(rightFighter, leftFighter);
      }
    }

    function fillIndicator(arenaFighter: IArenaFighter): void {
      const maxHitPoints: number = arenaFighter.fighter.health;
      const currentHitPoints: number = arenaFighter.currentHealth;
      const part = Math.floor((currentHitPoints / maxHitPoints) * 100);

      arenaFighter.healthIndicator.style.width = `${part > 0 ? part : 0}%`;
    }

    function keyDownHandler(keyEvent: KeyboardEvent) {
      if (keyEvent.repeat) return;

      makeAttack(keyEvent.code);
    }

  });
}

export function getDamage(attacker: IFighter, defender: IFighter): number {
  const attackDamage = getHitPower(attacker),
    blockDamage = getBlockPower(defender);
  const damage = attackDamage - blockDamage;

  return Math.max(damage, 0);
}

export function getHitPower(fighter: IFighter): number {
  const criticalHitChance = getRandomNumber(1);

  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter: IFighter): number {
  const dodgeChance = getRandomNumber(1);

  return fighter.defense * dodgeChance;
}

function getRandomNumber(minimumNumber: number): number {
  return Math.random() + minimumNumber;
}
