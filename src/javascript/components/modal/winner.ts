import {IFighter} from '../../helpers/fighterTypes';
import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter:IFighter):void {
  showModal({
    title: fighter.name + ' win',
    bodyElement: createFighterImage(fighter)
  });
}
